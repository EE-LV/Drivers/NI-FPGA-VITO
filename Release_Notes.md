Release Notes for the NI-FPGA-VITO Project
==========================================
This LabVIEW project "NI FPGA-VITO.lvproj" is used to implement VITO specific trigger and acquisition patterns.

Currently used development SW is LabVIEW 19.0.1f3

Version 0.0.0.0
===============
The project was just started. There is the master branch with license only.

Release 1.0.2.14
================
H.Brand@gsi.de, D.Neidherr@gsi.de, Marcus.Jankowski@cern.ch, Jared.Croese@cern.ch
VITO Scope with 8 channels was successfully tested with Co-60 source.
   - external trigger PFI0
   - Gated data with timestamp
   - Pulse properties with 7 parameters
     - WCM: Window Coincicence Mask  
	 - Integral
	 - Maximum
	 - Time
	 - ToT: Time over Threshold
	 - Counter

Additional feature:
   - Delayed pulse generation, once after PFI0 trigger.
   
   