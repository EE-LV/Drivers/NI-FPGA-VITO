VITO Scope README
=================
This LabVIEW project "NI FPGA-VITO.lvproj" is used to implement VITO specific trigger and acquisition patterns.

Currently used development SW is LabVIEW 20.0.1 (32-bit)

Related documents and information
=================================
- README.md
- Release_Notes.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/Drivers/NI-FPGA-VITO

External Dependencies
=====================
- LabVIEW Instrument Design Libraries für rekonfigurierbare Oszilloskope: https://www.ni.com/de-de/support/downloads/drivers/download.labview-instrument-design-libraries-for-reconfigurable-oscilloscopes.html#345644

Getting started:
=================================
- Install external dependencies, e.g. driver.
- Clone this repository, if not alread done.
- Switch to the desired branch.
- Open project "NI FPGA-VITO.lvproj"
- Compile VITO_Scope_FPGA.lvlib:Main.vi using the build specifications under FPGA target.

Known issues:
=============

Author: H.Brand@gsi.de, D.Neidherr@gsi.de, Marcus.Jankowski@cern.ch, Jared.Croese@cern.ch

Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.